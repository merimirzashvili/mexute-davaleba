package com.example.merimirzashvili5davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private lateinit var buttonSubmit: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        registerListeners()
    }


    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        buttonSubmit = findViewById(R.id.buttonSubmit)

    }
    private fun registerListeners(){
        buttonSubmit.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val repeatpassword = editTextRepeatPassword.text.toString()


            if(email.contains('@') && password.length >=9 && password.contains("0") || password.contains("1")||password.contains
                    ("2")||password.contains("3") ||password.contains("4") ||password.contains("5") ||password.contains("6")
                ||password.contains("7") ||password.contains("8") ||password.contains("9") && password.contains("!") ||password.contains("@") ||
                password.contains("#") ||password.contains("$") ||password.contains("%") ||password.contains("&") ||
                password.contains("*") ||password.contains(".") ||password.contains(",") ||password.contains("?"))
                Toast.makeText(this,"You are Registered!",Toast.LENGTH_SHORT).show()


            if ( password != repeatpassword || email.isEmpty() || password.isEmpty() || repeatpassword.isEmpty())

                Toast.makeText(this,"Empty!",Toast.LENGTH_SHORT).show()
              return@setOnClickListener


}


         FirebaseAuth.getInstance().createUserWithEmailAndPassword(email , password)
             .addOnCompleteListener {  task->
                 if(task.isSuccessful){
                     finish()
             }else{
                     Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show()
                 }




        }
    }

}